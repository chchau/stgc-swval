#!/bin/python

import numpy as np
import matplotlib.pyplot as plt
#from scipy.special import gamma
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

if __name__ == '__main__':
  data = np.loadtxt("hitinfo.txt", usecols=(1,3))
  stepLength = data[:,0]
  kinEnergy = data[:,1]
  #print(dist[:10])

  ##plt.clf()
  #fig, ax = plt.subplots()
  #te,tbin,tp = ax.hist(stepLength, bins=50, range=(0.,50.), density=True, log=True)
  #ax.set_xlabel('Step Length [mm]')
  #ax.xaxis.set_minor_locator(MultipleLocator(2))
  #ax.grid(True)
  #plt.show()

  fig, ax = plt.subplots()
  te,tbin,tp = ax.hist(kinEnergy, bins=20, range=(0.,20.), density=True, log=True)
  ax.set_xlabel('Kinetic Energy [MeV]')
  #ax.xaxis.set_minor_locator(MultipleLocator(20))
  #ax.grid(True)
  plt.show()
